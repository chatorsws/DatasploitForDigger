import sys
import json

class Payload(object):
    def __init__(self, j):
        self.__dict__ = json.loads(j)

def CreateJsonNode(data, source, note, typeOfData, links, master):
    json = "{"
    json += "\"Data\" : \"" + data + "\","
    json += "\"Source\" : \"" + source + "\","
    json += "\"Note\" : \"" + note + "\","
    json += "\"TypeOfData\" : \"" + typeOfData + "\""
    if master == True or len(links) > 1:
        json += ","
        json += "\"Link\" : ["
        if master == True:
            json += "{\"Uid\" : \"master\"}"
            if len(links) > 0:
                json += ","
        if len(links) > 0:
            i = 0
            for link in links:
                if i == len(links) - 1:
                    json += "{\"Uid\" : \"" + "mdr" + "\"}"
                else:
                    json += "{\"Uid\" : \"" + "mdr" + "\"},"
                i += 1
        json += "]"
    json += "}"
    return json


if len(sys.argv) != 3:
    print "Error need 2 arg"
    sys.exit(0)
    
jsonFileName = sys.argv[1]
nameDomain = sys.argv[2]

# Open the file with the json
fichier = open(jsonFileName, "r")
jsonText = fichier.read()
fichier.close()

data = Payload(jsonText)
listJsonData = list()
#data+typeOfdata POUR LES LINKS

#domain_censys
if data.domain_censys is not None:
	a = dict(data.domain_censys)

	source = "Censys - info croustillante"
	for element in data.domain_censys:
		note = "Titre de l'ip : " + element["title"] + " | Les ports ouverts sur l\'adresse sont " + element["protocols"]
		listJsonData.append(CreateJsonNode(element["ip"], source, note, "Adresse Ip", [], True))
	
#domain_dnsrecords
a = dict(data.domain_dnsrecords)

source = "DnsRecord - Obtenir adresse Ip"
if "A Records" in a:
	for element in a["A Records"]:
		listJsonData.append(CreateJsonNode(element, source, "L\'adresse IpV4 du nom de domaine", "Adresse IpV4", [], True))
if "AAAA Records" != "":
	listJsonData.append(CreateJsonNode(a["AAAA Records"], source, "L\'adresse IpV6 du nom de domaine", "Adresse IpV6", [], True))

source = "DnsRecord - Obtenir serveurs records"
if "CNAME Records" in a:
	listJsonData.append(CreateJsonNode(a["CNAME Records"], source, "L\'alias utilise par le serveur DNS", "Domain", [], True))
if "Name Server Records" in a:
	for element in a["Name Server Records"]:
		listJsonData.append(CreateJsonNode(element, source, "Prestataire de service DNS", "Domain", [], True))
	
#domain_emailhunter
if data.domain_emailhunter[0] != False:
	source = "EmailHunter - Recuperer les mails du domain"
	for element in data.domain_emailhunter: 
		listJsonData.append(CreateJsonNode(element, source, "Mail appartenant au domain", "Mail", [], True))
	
#domain_subdomains
source = "SubDomains - Trouver les sous-domaines d'un domaine"
for element in data.domain_subdomains: 
	listJsonData.append(CreateJsonNode(element, source, "Sous-domaine", "Domain", [], True))

#domain_whois
a = dict(data.domain_whois)

source = "Whois - Trouver date, actualisation et expiration d'un domaine"
if "creation_date" in a:
	listJsonData.append(CreateJsonNode(a["creation_date"], source, "Date de creation du domaine", "Date", [], True))
if "expiration_date" in a:
	listJsonData.append(CreateJsonNode(a["expiration_date"], source, "Date d'expiration du domaine", "Date", [], True))
if "updated_date" in a:
	listJsonData.append(CreateJsonNode(a["updated_date"], source, "Date de la derniere actualisation du domaine", "Date", [], True))

source = "Whois - Trouver le nom de l'enregistreur du domaine"
if "registrar" in a:
	listJsonData.append(CreateJsonNode(a["registrar"], source, "Nom ou organisation qui a reserve le nom de domaine", "Nom", [], True))

source = "Whois - Trouver les mails qui ont sevis a enregistrer le domaine"
if "emails" in a:
	for element in a["emails"]:
		listJsonData.append(CreateJsonNode(element, source, "Mail qui ont servis a enregistrer le nom de domaine", "Mail", [], True))

source = "Whois - Trouver les serveur du prestataire du domaine"
if "name_servers" in a:
	for element in a["name_servers"]:
		listJsonData.append(CreateJsonNode(element, source, "Serveur prestataire de service DNS", "Domain", [], True))

# The json result
jsonResult = ""
for element in listJsonData:
    jsonResult += element + ","
jsonResult = jsonResult[0:len(jsonResult)-1]

#save in file
print (jsonResult)
fichier = open(jsonFileName, "w")
fichier.write(jsonResult)
fichier.close()

